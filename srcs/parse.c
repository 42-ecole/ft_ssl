/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/12 20:37:46 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/15 19:04:34 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl.h"

t_list	*parse_args(t_list *r_args)
{
	t_list	*args;
	int		file_count;
	char	*arg;
	size_t	i;

	args = new_list(r_args->count + 2, sizeof(t_arg));
	args->add_f(args, new_arg(E_STDIN, NULL));
	file_count = manage_std_str_args(r_args, &args);
	i = r_args->count - file_count;
	while (i < r_args->count)
	{
		g_alone = false;
		arg = (char *)r_args->index_of(r_args, i);
		if (arg[0] != '-')
			args->add_f(args, new_arg(E_FILE, arg));
		++i;
	}
	return (args);
}

int		manage_std_str_args(t_list *r_args, t_list **args)
{
	t_list	*tmp;
	char	*arg;
	size_t	i;
	int		s;
	
	s = 0;
	i = 0;
	tmp = new_list(r_args->count + 1, (*args)->content_size);
	while (i < r_args->count)
	{
		arg = (char *)r_args->index_of(r_args, i);
		if (arg[0] == '-')
		{
			if (ft_strchr(arg, 's'))
				++s;
		}
		else
		{
			if (s > 0)
			{
				tmp->add_f(tmp, new_arg(E_STRING, arg));
				g_alone = false;
				--s;
			}
			else
				break;
		}
		++i;
	}
	if (s)
		exit_with_err("Error ft_ssl: excepted -s \"[input]\"");
	(*args)->add_range(*args, tmp);
	free(tmp);
	return (r_args->count - i);
}
