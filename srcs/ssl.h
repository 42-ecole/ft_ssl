#ifndef SSL_H
# define SSL_H

# include "libft.h"

# define F1(x, y, z) ((x & y) | (~x & z))
# define F2(x, y, z) ((x & z) | (y & ~z))
# define F3(x, y, z) (x ^ y ^ z)
# define F4(x, y, z) (y ^ (x | ~z))

# define RROT(X, N) ((X >> N) | (X << (32 - N)))
# define LROT(X, N) ((X << N) | (X >> (32 - N)))

# define S1(X) ((RROT(X, 6)) ^ (RROT(X, 11)) ^ (RROT(X, 25)))
# define CH(x, y, z) ((x & y) ^ (~ x & z))
# define S0(X) ((RROT(X, 2)) ^ (RROT(X, 13)) ^ (RROT(X, 22)))
# define MAJ(x, y, z) ((x & y) ^ (x & z) ^ (y & z))

# define BLOCK_SIZE 512

typedef unsigned char t_byte;

typedef enum	s_hash_type
{
	md5,
	sha256,
}				t_hash_type;

typedef struct s_data
{
	uint32_t	*t;
	uint32_t	a;
	uint32_t	b;
	uint32_t	c;
	uint32_t	d;
	uint32_t	e;
	uint32_t	f;
	uint32_t	g;
	uint32_t	h;
	uint32_t	h0;
	uint32_t	h1;
	uint32_t	h2;
	uint32_t	h3;
	uint32_t	h4;
	uint32_t	h5;
	uint32_t	h6;
	uint32_t	h7;
	uint32_t	s0;
	uint32_t	s1;
	uint32_t	s2;
	uint32_t	s3;
	uint32_t	s4;
	uint32_t	s5;
	uint32_t	tmp;
	int			blocks_count;
	int			len;
}				t_data;

/*
	Temp property - result. TODO lst_sort with comparator. Then refactor.
*/
typedef struct	s_hash
{
	t_hash_type	type;
	t_data		data;
	char		*(*compute)(t_list *bytes);
}				t_crypt_service;

typedef struct	s_flag
{
	char		*tag;
	void		*(*action)(void *data);
}				t_flag;

typedef struct	s_flags
{
	bool		mupltiple;
	t_list		*lst;
	void		(*add)(struct s_list *, void *);
}				t_flags;

typedef enum	arg_type
{
	E_NONE,
	E_STDIN,
	E_STRING,
	E_FILE
}				e_arg_type;

typedef struct	s_arg
{
	e_arg_type	type;
	void		*data;
}				t_arg;

t_flags			*g_flags;
bool			g_alone;

t_arg				*new_arg(e_arg_type type, void *data);
void				manage_arg(void *obj, void *data);

t_flags				*parse_flags(t_flags *flags, t_list *args);

t_crypt_service		*define_crypt_service(t_list *args);

char				*new_md5(t_list *bytes);
char				*new_sha256(t_list *bytes);

void				add_bytes(t_list *bytes, char *str);
char				*ft_utoa_base(uint32_t n, int base);
uint32_t			reverse_uint32(uint32_t n);

void				exit_with_err(char *str);

int					read_bytes(t_list **bytes, int fd);
int					read_bytes_str(t_list **bytes, char *str);
int					read_file(t_list **bytes, char *path);

char				*printf_wrap(char *data);

int					get_short_flags(t_flags *flags);
bool				flags_manage(t_flags **flags, t_list *args);
void				free_flags(t_flags *flags);

t_list				*parse_args(t_list *r_args);
int					manage_std_str_args(t_list *r_args, t_list **args);

void				compute_format_hash(t_crypt_service *cs,
						t_list *bytes, t_arg *arg, int flags);

void				print_str(void *obj, void *data);
void				print_flag(void *obj, void *data);

#endif