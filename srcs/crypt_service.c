/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   crypt_service.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/06 16:10:50 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/12 21:11:18 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl.h"

uint32_t	reverse_uint32(uint32_t n)
{
	return ((n >> 24) | ((n & 0xff0000) >> 8) |
		((n & 0xff00) << 8) | (n << 24));
}

t_crypt_service		*new_crypt_service(t_hash_type hash_type)
{
	t_crypt_service		*cs;

	if (!(cs = ft_memalloc(sizeof(t_crypt_service))))
		return NULL;
	cs->type = hash_type;
	if (hash_type == md5)
		cs->compute = new_md5;
	else if (hash_type == sha256)
		cs->compute = new_sha256;
	return (cs);
}

t_crypt_service		*define_crypt_service(t_list *args)
{
	t_crypt_service		*cs;

	if (!ft_strcmp(args->index_of(args, 1), "md5"))
		cs = new_crypt_service(md5);
	else if (!ft_strcmp(args->index_of(args, 1), "sha256"))
		cs = new_crypt_service(sha256);
	else
	{
		ft_printf("ft_ssl: Error: '%s' is an invalid command.\n\nStandard \
commands:\n\nMessage Digest commands:\nmd5\nsha256\n\nCipher \
commands:\n", args->index_of(args, 1));
		exit(1);
	}
	return cs;
}
