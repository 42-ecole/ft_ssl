/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   block.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/08 13:59:48 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/08 14:04:22 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl.h"
#include "sha256.h"

void		shift_block(t_data *data, uint32_t *block)
{
	int j;

	j = 16;
	while (j < 64)
	{
		data->s0 = RROT(block[j - 15], 7) ^
			RROT(block[j - 15], 18) ^ (block[j - 15] >> 3);
		data->s1 = RROT(block[j - 2], 17) ^
			RROT(block[j - 2], 19) ^ (block[j - 2] >> 10);
		block[j] = block[j - 16] + data->s0 + block[j - 7] + data->s1;
		j++;
	}
	data->a = data->h0;
	data->b = data->h1;
	data->c = data->h2;
	data->d = data->h3;
	data->e = data->h4;
	data->f = data->h5;
	data->g = data->h6;
	data->h = data->h7;
}

uint32_t	*new_block(t_list *bytes, int index)
{
	uint32_t	*block;

	block = ft_memalloc(BLOCK_SIZE);
	ft_bzero(block, BLOCK_SIZE);
	ft_memcpy(block, bytes->objs + index * 16 * sizeof(uint32_t), BLOCK_SIZE);
	return (block);
}
