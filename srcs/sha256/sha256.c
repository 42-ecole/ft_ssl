/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha256.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/06 16:11:20 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/12 22:13:38 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl.h"
#include "sha256.h"

static void		init_mdbuf(t_data *data)
{
	data->h0 = 0x6a09e667;
	data->h1 = 0xbb67ae85;
	data->h2 = 0x3c6ef372;
	data->h3 = 0xa54ff53a;
	data->h4 = 0x510e527f;
	data->h5 = 0x9b05688c;
	data->h6 = 0x1f83d9ab;
	data->h7 = 0x5be0cd19;
}

static void		swap_words(t_data *data, uint32_t chunk, uint32_t t)
{
	data->s1 = S1(data->e);
	data->s2 = CH(data->e, data->f, data->g);
	data->s3 = data->h + data->s1 + data->s2 + t + chunk;
	data->s0 = S0(data->a);
	data->s4 = MAJ(data->a, data->b, data->c);
	data->s5 = data->s0 + data->s4;
	data->h = data->g;
	data->g = data->f;
	data->f = data->e;
	data->e = data->d + data->s3;
	data->d = data->c;
	data->c = data->b;
	data->b = data->a;
	data->a = data->s3 + data->s5;
}

static void		compute_hash(t_list *bytes, t_data *data)
{
	uint32_t	*block;
	int			i;
	int			j;
	
	i = 0;
	while (i < data->blocks_count)
	{
		block = new_block(bytes, i);
		shift_block(data, block);
		j = -1;
		while (++j < 64)
			swap_words(data, block[j], g_t[j]);
		data->h0 += data->a;
		data->h1 += data->b;
		data->h2 += data->c;
		data->h3 += data->d;
		data->h4 += data->e;
		data->h5 += data->f;
		data->h6 += data->g;
		data->h7 += data->h;
		free(block);
		i++;
	}
}

static int		stream_alighment(t_list *bytes, t_crypt_service *cs)
{
	t_byte 		zero;
	t_byte 		one;
	uint32_t	len;
	uint32_t	tmp;

	zero = 0x00;
	one = 0x80;
	tmp = 0;
	
	len = (uint32_t)(bytes->count * 8);
	cs->data.blocks_count = 1 + ((len + 16 + 64) / 512);
	bytes->add_size(bytes, &one, sizeof(t_byte));
	while (bytes->count % 64 != 56)
		bytes->add_size(bytes, &zero, sizeof(t_byte));
	int i = 0;
	while (i < (cs->data.blocks_count * 16) - 1)
	{
		uint32_t *num = CAST_TYPE(uint32_t, bytes->index_of(bytes, i * sizeof(uint32_t)));
		*num = reverse_uint32(*num);
		i++;
	}
	bytes->add_size(bytes, &tmp, sizeof(uint32_t));
	bytes->add_size(bytes, &len, sizeof(uint32_t));
	return (0);
}

char		*printf_wrap(char *data)
{
	char	*ret;

	ret = ft_asrprintf("%08s", data, 16);
	free(data);
	return (ret);
}

char			*new_sha256(t_list *bytes)
{
	t_crypt_service	cs;

	init_mdbuf(&(cs.data));
	stream_alighment(bytes, &cs);
	compute_hash(bytes, &(cs.data));

	return ft_joinargs(8,
		printf_wrap(ft_utoa_base(cs.data.h0, 16)),
		printf_wrap(ft_utoa_base(cs.data.h1, 16)),
		printf_wrap(ft_utoa_base(cs.data.h2, 16)),
		printf_wrap(ft_utoa_base(cs.data.h3, 16)),
		printf_wrap(ft_utoa_base(cs.data.h4, 16)),
		printf_wrap(ft_utoa_base(cs.data.h5, 16)),
		printf_wrap(ft_utoa_base(cs.data.h6, 16)),
		printf_wrap(ft_utoa_base(cs.data.h7, 16)));

}
