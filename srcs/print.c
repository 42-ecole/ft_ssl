/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/12 20:50:18 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/12 22:45:54 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl.h"

char	*get_arg_name(t_arg *arg)
{
	if (arg->type == E_STDIN)
		return ft_asrprintf("stdin", arg->data);
	else if (arg->type == E_STRING)
		return ft_asrprintf("\"%s\"", arg->data);
	else if (arg->type == E_FILE)
		return ft_strdup(arg->data);
	return ("");
}

char	*get_hash_name(t_crypt_service *cs)
{
	if (cs->type == md5)
		return "MD5";
	else if (cs->type == sha256)
		return "SHA256";
	return ("");
}

// char		*printf_wrap_free(t_format *f, char *data)
// {
// 	char	*ret;

// 	ret = ft_printf(f, data);
// 	free(data);
// 	return (ret);
// }

void	compute_format_hash(t_crypt_service *cs,
				t_list *bytes, t_arg *arg, int flags)
{
	char	*hash;
	char	*name;
	char	*str;

	str = NULL;
	name = get_arg_name(arg);
	if (arg->type == E_STDIN && (get_bit(flags, 0)))
	{
		str = ft_lsttostr(bytes);
		printf("%s\n", str);
	}
	hash = cs->compute(bytes);
	if (get_bit(flags, 2))
		ft_printf("%s\n", hash);
	else
	{
		if (get_bit(flags, 1))
			ft_printf("%s %s\n",
				hash,
				name);
		else
				ft_printf("%s (%s) = %s\n",
					get_hash_name(cs),
					name,
					hash);
	}
	if (hash)
		free(hash);
	if (name)
		free(name);
	if (str)
		free(str);
}
