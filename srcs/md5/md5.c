/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/04 15:31:23 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/15 19:00:57 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl.h"
#include "md5.h"

static void	init_mdbuf(t_crypt_service *cs)
{
	cs->data.h0 = 0x67452301;
	cs->data.h1 = 0xefcdab89;
	cs->data.h2 = 0x98badcfe;
	cs->data.h3 = 0x10325476;
}

static void	stream_alignment(t_list *bytes)
{
	t_byte 		zero;
	t_byte 		one;
	uint64_t	len;

	zero = 0x00;
	one = 0x80;
	len = (uint64_t)(bytes->count * sizeof(uint64_t));
	bytes->add_size(bytes, &one, sizeof(t_byte));
	while (bytes->count % 64 != 56)
		bytes->add_size(bytes, &zero, sizeof(t_byte));
	bytes->add_size(bytes, &len, sizeof(uint64_t));
}

static void	figure_out_hash(t_crypt_service *cs, int i)
{
	if (i < 16)
	{
		cs->data.f = F1(cs->data.b, cs->data.c, cs->data.d);
		cs->data.g = i;
	}
	else if (i < 32)
	{
		cs->data.f = F2(cs->data.b, cs->data.c, cs->data.d);
		cs->data.g = (5 * i + 1) % 16;
	}
	else if (i < 48)
	{
		cs->data.f = F3(cs->data.b, cs->data.c, cs->data.d);
		cs->data.g = (3 * i + 5) % 16;
	}
	else
	{
		cs->data.f = F4(cs->data.b, cs->data.c, cs->data.d);
		cs->data.g = (7 * i) % 16;
	}
	cs->data.tmp = cs->data.d;
	cs->data.d = cs->data.c;
	cs->data.c = cs->data.b;
	cs->data.b = cs->data.b + LROT((cs->data.a + cs->data.f + g_t[i] + cs->data.t[cs->data.g]), g_s[i]);
	cs->data.a = cs->data.tmp;
}

static void	compute_hash(t_list *words, t_crypt_service *cs)
{
	size_t	n;
	int		i;

	n = 0;
	while (n < words->count)
	{
		cs->data.t = (uint32_t *)words->index_of(words, n);
		cs->data.a = cs->data.h0;
		cs->data.b = cs->data.h1;
		cs->data.c = cs->data.h2;
		cs->data.d = cs->data.h3;
		i = -1;
		while (++i < 64)
			figure_out_hash(cs, i);
		cs->data.h0 += cs->data.a;
		cs->data.h1 += cs->data.b;
		cs->data.h2 += cs->data.c;
		cs->data.h3 += cs->data.d;

		n += 64;
	}
}

char		*new_md5(t_list *bytes)
{
	t_crypt_service	cs;

	init_mdbuf(&cs);
	stream_alignment(bytes);
	compute_hash(bytes, &cs);

	return ft_joinargs(4,
		printf_wrap(ft_utoa_base(reverse_uint32(cs.data.h0), 16)),
		printf_wrap(ft_utoa_base(reverse_uint32(cs.data.h1), 16)),
		printf_wrap(ft_utoa_base(reverse_uint32(cs.data.h2), 16)),
		printf_wrap(ft_utoa_base(reverse_uint32(cs.data.h3), 16)));
}
