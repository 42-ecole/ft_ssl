/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   debug.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/08 14:15:33 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/15 19:00:32 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl.h"

void		exit_with_err(char *str)
{
	printf("%s\n", str);
	exit(1);
}

// void		print_el(void *obj, void *data)
// {
// 	t_byte		byte;

// 	byte = *CAST_TYPE(t_byte, obj);
// 	int j = 0;
// 	while (j < 8)
// 	{
// 		printf("%c", (byte & 0x80) ? '1' : '0');
// 		byte <<= 1;
// 		++j;
// 	}
// 	printf("\n");
// 	// printf("%c", byte);
// }

// void		print_uint32(void *obj, void *data)
// {
// 	uint32_t	num;

// 	num = *CAST_TYPE(uint32_t, obj);
// 	printf("%d ", num);
// }

// void		print_bytes2(t_byte *bytes, int len)
// {
// 	t_byte *cp;
// 	int	i;
// 	int	j;

// 	i = 0;
// 	len = ft_strlen((char *)bytes);
// 	cp = ft_memalloc(sizeof(t_byte) * len);
// 	ft_memcpy(cp, bytes, sizeof(t_byte) * len);
// 	while (i < len)
// 	{
// 		j = 0;
// 		while (j < 8)
// 		{
//     		printf("%c", (cp[i] & 0x80) ? '1' : '0');
//     		cp[i] <<= 1;
// 			++j;
// 		}
//   		printf("\n");
// 		++i;
// 	}
//   	printf("\n");
// }

// void	print_str(void *obj, void *data)
// {
// 	char 	*str;

// 	str = (char *)obj;
// 	ft_printf("%s\n", str);
// }

// void	print_flag(void *obj, void *data)
// {
// 	t_flag *flag = obj;
// 	ft_printf("%s\n", flag->tag);
// }
