/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bytes.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/11 13:38:02 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/15 22:26:54 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

void		read_data(char **ptr, int fd)
{
	char	c[2];
	int		verif;
	char	*str;
	char	*tmp;

	c[1] = 0;
	verif = 42;
	str = ft_strdup("");
	while (verif)
	{
		verif = read(fd, c, 1);
		if (!verif)
			break ;
		tmp = str;
		str = ft_strjoin(str, c);
		free(tmp);
	}
	ptr[0] = str;
}

void		add_bytes(t_list *bytes, char *str)
{
	int		len;
	size_t	i;

	i = 0;
	len = ft_strlen(str);
	while (i < ft_strlen(str))
	{
		unsigned char c = str[i];
		bytes->add(bytes, &c);
		++i;
	}
}

int			read_bytes(t_list **bytes, int fd)
{
	char		*stdin;

	read_data(&stdin, fd);
	*bytes = new_list(200000, sizeof(t_byte));
	add_bytes(*bytes, stdin);
	free(stdin);
	return (0);
}

int			read_bytes_str(t_list **bytes, char *str)
{
	add_bytes(*bytes, str);
	return (0);
}

int			read_file(t_list **bytes, char *path)
{
	int		fd;
	int			ret;
	char		buf[BUFF_SIZE + 1];

	fd = open(path, O_RDONLY);
	if (fd < 0)
	{
		ft_printf("ft_ssl: md5: %s: No such file or directory\n", path);
		return (1);
	}
	else if (fd > 0)
	{
		while ((ret = read(fd, buf, BUFF_SIZE)) > 0)
		{
			buf[ret] = '\0';
			add_bytes(*bytes, buf);
		}
	}
	else
		return (1);
	if ((*bytes)->count)
	{
		close(fd);
		return (0);
	}
	return (0);
}

