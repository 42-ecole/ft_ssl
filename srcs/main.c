/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/04 13:17:22 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/15 20:17:54 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl.h"

int		main(int argc, char *argv[])
{
	t_crypt_service		*cs;
	t_list				*args;
	t_list				*type_args;

	g_alone = true;
	if (argc == 1)
		exit_with_err("usage: ft_ssl command [-pqrs] [command args]\n");
	args = ft_strstolst(argv, argc);
	cs = define_crypt_service(args);
	args = ft_replace(args, new_sublist(args, 2, args->count - 1));
	type_args = parse_args(args);
	flags_manage(&g_flags, args);
	foreach(type_args->enumerable, type_args, manage_arg, cs);
	free(cs);
	free_flags(g_flags);
	free(type_args);
	free(args);
	return 0;
}