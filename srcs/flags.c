/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flags.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/08 20:24:30 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/15 19:00:09 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl.h"

bool	cmp_flag(void *obj, void *argv)
{
	t_flag	*flag;
	char	*arg;
	char	*str;
	int		result;

	flag = obj;
	str = argv;
	result = -1;
	arg = ft_memalloc(2);
	arg[0] = str[0];
	arg[1] = '\0';
	result = ft_strcmp(flag->tag, arg);
	free(arg);
	if (!result)
		return (true);
	return (false);
}

bool		has_long_flag(char *flags)
{
	int		i;
	char	last_chr;

	i = 0;
	while (flags[++i])
	{
		last_chr = flags[i - 1];
		if (flags[i] == '-' && flags[i] == last_chr)
			return (true);
	}
	return (false);
}

t_flags		*new_flags(size_t count)
{
	t_flags *flags;

	flags = ft_memalloc(sizeof(t_flags));
	flags->lst = new_list(count, sizeof(t_flag));
	flags->add = flags->lst->add_f;
	return (flags);
}

typedef struct	s_flags_container
{
	t_flags		*active_flags;
	t_flags		*flags;
}						t_flags_container;

void	add_difference(t_list *dst, t_list *src)
{
	size_t		i;
	size_t		j;
	bool		trigger;
	t_flag		*flag;
	t_flag		*flag2;

	i = 0;
	while (i < src->count)
	{
		flag = CAST_TYPE(t_flag , src->objs + i * src->content_size); 
		j = 0;
		trigger = true;
		while (j < dst->count)
		{
			flag2 = CAST_TYPE(t_flag, dst->index_of(dst, j));
			if (flag->tag == flag2->tag)
			{
				trigger = false;
				break;
			}
			++j;
		}
		if (trigger)
			dst->add(dst, flag);
		++i;
	}
}

void	parse_flags_arg(void *arg, void *data)
{
	t_flags_container	*con;
	char				*raw_flag;
	t_list				*tmp;
	int					len;

	con = (t_flags_container *)data;
	raw_flag = (char *)arg;
	if (raw_flag[0] != '-')
		return ;
	if (has_long_flag(raw_flag))
		exit_with_err("Error ft_ssl: Illegal option");
	raw_flag = ft_strdel_chars(raw_flag, 2, "-", " ");
	if (ft_strisdupl(raw_flag))
		exit_with_err("Error ft_ssl: Duplicate option");
	len = ft_strlen(raw_flag);
	while (len)
	{
		tmp = ft_select(con->flags->lst->enumerable, 
						con->flags->lst, cmp_flag, &(raw_flag[--len]));
		if (!tmp->count)
			exit_with_err("Error ft_ssl: Undefiend option");
		add_difference(con->active_flags->lst, tmp);
		free(tmp);
	}
	free(raw_flag);
}

t_flags		*parse_flags(t_flags *flags, t_list *args)
{
	t_flags_container	*con;
	t_flags		*active_flags;

	con = ft_memalloc(sizeof(t_flags_container));
	con->active_flags = new_flags(flags->lst->capacity);
	con->flags = flags;
	foreach(args->enumerable, args, parse_flags_arg, con);
	active_flags = con->active_flags;
	free(con);
	return (active_flags);
}

void		free_flags(t_flags *flags)
{
	free(flags->lst);
	free(flags);
}

static t_flag		*new_flag(char *tag, void *(*action)(void *data))
{
	t_flag	*flag;

	flag = ft_memalloc(sizeof(t_flag));
	flag->tag = tag;
	flag->action = action;
	return (flag);
}

static bool	is_tag_name(void *obj, void *data)
{
	t_flag	*flag;
	char	*tag;

	tag = (char *)data;
	flag = (t_flag *)obj;
	if (!ft_strcmp(flag->tag, tag))
		return (true);
	return (false);
}

int		get_short_flags(t_flags *flags)
{
	t_list	*has_r;
	t_list	*has_q;
	t_list	*has_p;
	int		s_flags;

	has_q = NULL;
	has_r = NULL;
	has_p = NULL;
	s_flags = 0;
	if (flags->lst->count)
	{
		has_r = ft_select(flags->lst->enumerable,
			g_flags->lst, is_tag_name, "r");
		has_q = ft_select(flags->lst->enumerable,
			g_flags->lst, is_tag_name, "q");
		has_p = ft_select(flags->lst->enumerable,
			g_flags->lst, is_tag_name, "p");
	}
	if (has_p && has_p->count)
		s_flags = set_bit(s_flags, 0);
	if (has_r && has_r->count)
		s_flags = set_bit(s_flags, 1);
	if (has_q && has_q->count)
		s_flags = set_bit(s_flags, 2);
	if (flags->lst->count)
	{
		free(has_p);
		free(has_q);
		free(has_r);
	}
	return s_flags;
}


static t_flags		*init_flags()
{
	t_flags		*flags;

	flags = new_flags(4);
	flags->add(flags->lst, new_flag("p", NULL));
	flags->add(flags->lst, new_flag("q", NULL));
	flags->add(flags->lst, new_flag("r", NULL));
	flags->add(flags->lst, new_flag("s", NULL));
	return (flags);
}


bool	flags_manage(t_flags **flags, t_list *args)
{
	t_flags		*i_flags;

	i_flags = init_flags();
	*flags = parse_flags(i_flags, args);
	free_flags(i_flags);
	if ((*flags)->lst->count)
		return (true);
	return (false);
}
