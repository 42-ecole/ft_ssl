/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   argument.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/10 22:46:56 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/15 21:20:36 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl.h"

void	manage_arg(void *obj, void *data)
{
	t_crypt_service	*cs;
	t_arg			*arg;
	t_list			*bytes;
	int				status;
	int				s_flags;


	arg = obj;
	status = -1;
	cs = (t_crypt_service *)data;
	s_flags = get_short_flags(g_flags);
	bytes = new_list(200000, sizeof(t_byte));
	if (arg->type == E_STDIN && (get_bit(s_flags, 0) || g_alone))
		status = read_bytes(&(bytes), 0);
	else if (arg->type == E_STRING)
		status = read_bytes_str(&(bytes), arg->data);
	else if (arg->type == E_FILE)
		status = read_file(&(bytes), arg->data);
	if (!status)
		compute_format_hash(cs, bytes, arg, s_flags);
	free(bytes);
}

t_arg	*new_arg(e_arg_type type, void *data)
{
	t_arg	*arg;

	arg = ft_memalloc(sizeof(t_arg));
	arg->type = type;
	arg->data = data;
	return (arg);
}
