/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   time.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk>                              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/27 00:00:43 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/27 00:04:04 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "liblog.h"

uint64_t	local_time_offset(time_t secs)
{
    uint64_t diff;
    struct tm timeParts;
    memset(&timeParts, 0, sizeof(timeParts));
    struct tm *timeInfo = localtime_r( &secs, &timeParts );
    diff = mktime( timeInfo );

    memset(&timeParts, 0, sizeof(timeParts));
    timeInfo = gmtime_r ( &secs, &timeParts );
    diff -= mktime( timeInfo );
	return diff;
}

char		*get_current_time()
{
	struct tm 	*newtime;
	time_t		current;

	current = time(NULL);
	newtime = gmtime(&current);
	return (ft_joinargs(11, ft_itoa(newtime->tm_mday),
			":", ft_itoa(newtime->tm_mon + 1),
			":", ft_itoa(newtime->tm_year + 1900),
			" ", ft_itoa((newtime->tm_hour
			+ local_time_offset(current) / 3600) % 24),
			":", ft_itoa(newtime->tm_min),
			":", ft_itoa(newtime->tm_sec)));
}
