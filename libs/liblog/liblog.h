/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   liblog.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk>                              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/26 22:04:52 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/04 20:33:13 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBLOG_H
# define LIBLOG_H

/*
	Libft dependecy
*/
# include "libft.h"
# include <stdio.h>
# include <stdint.h>
# include <time.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <dirent.h>

# define DEFAULT_DATE_LEN 30
# define DEFAULT_MESSAGE_LEN 200

enum 			mes_state
{
	INFO,
	WARNING,
	ERROR,
	FATAL
};

typedef struct	s_log_record
{
	enum mes_state	state;
	char			*date;
	char			*message;
}				t_log_record;

typedef struct 	s_logger
{
	t_list			*log;
}				t_logger;

void			init_logger();
void			log_add(enum mes_state state, char *message);
void			log_print();
void			log_unload();

char			*get_current_time();

#endif