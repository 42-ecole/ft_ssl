NAME := ft_ssl
 
CC := gcc
CC_DEBUG := gcc -g
FLAGS = -Wall -Wextra -Werror
DEL := rm -rf
SRCS_PATH := ./srcs

#-----------SOURCES---------------#
SRC := main.c \
		md5/md5.c \
		sha256/sha256.c \
		sha256/block.c \
		crypt_service.c \
		debug.c \
		flags.c \
		argument.c \
		bytes.c \
		parse.c \
		print.c

SRC_DIR := $(addprefix $(SRCS_PATH)/, $(SRC))

OBJ := $(SRC_DIR:.c=.o)
#----------------------------------#


#----------LIBRARIES--------------#
LIBFTDIR := libs/libft
LIBFTINC := .
LIBFT := ft
FTMAKE := +make -sC $(LIBFTDIR)/$(LIBFTINC)

LIBLOGDIR := libs/liblog
LIBLOGINC := .
LIBLOG := log
LOGMAKE := +make -sC $(LIBLOGDIR)/$(LIBLOGINC)

# LIBPRINTFDIR := libs/printf
# LIBPRINTFINC := includes
# LIBPRINTF := printf
# PRINTFMAKE := +make -sC $(LIBPRINTFDIR)/$(LIBPRINTFINC)

LIBS := -L $(LIBLOGDIR) -l $(LIBLOG) -L $(LIBFTDIR) -l $(LIBFT)

INCLUDES := -I $(SRCS_PATH) \
			-I $(SRCS_PATH)/sha256 \
			-I $(SRCS_PATH)/md5 \
			-I $(LIBFTDIR)/$(LIBFTINC) \
			-I $(LIBLOGDIR)/$(LIBLOGINC)
#----------------------------------#


#-----------STYLES-----------------#
NONE = \033[0m
INVERT := \033[7m
GREEN := \033[32m
RED := \033[31m
SUCCESS := [$(GREEN)✓$(NONE)]
SUCCESS2 := [$(INVERT)$(GREEN)✓$(NONE)]
APPOK := $(INVERT)$(GREEN)Compiled ✓$(NONE)$(INVERT):$(NAME) has been successfully compiled.$(NONE)
APPDELETED := $(INVERT)$(RED)Removed ✓$(NONE)$(INVERT):$(NAME) has been successfully removed.$(NONE)
#----------------------------------#

all:
	@make -s -j4 $(NAME);

$(OBJ): %.o: %.c
	@echo -n $(NAME):' $@: '
	@$(CC) -c $(FLAGS) $(INCLUDES) $< -o $@
	@echo "$(SUCCESS)"

$(LIBFT):
	@$(FTMAKE)

$(LIBLOG):
	@$(LOGMAKE)

$(NAME):  $(OBJ) $(LIBFT) $(LIBLOG)
	@$(CC) $(OBJ) $(LIBS) -o $(NAME)
	@echo "$(APPOK)"

debug:
	$(CC_DEBUG) $(SRC_DIR) $(LIBS) $(INCLUDES) -g -o $(NAME)

$(CC_DEBUG):
	@$(eval CC=$(CC_DEBUG))
	debug_all: $(CC_DEBUG) pre
	@echo "$(INVERT)$(NAME) $(GREEN)ready for debug.$(NONE)"
	debug: $(CC_DEBUG) all
	@echo "$(INVERT)$(NAME) $(GREEN)ready for debug.$(NONE)"

clean:
	@$(DEL) $(OBJ)
	@$(FTMAKE) clean

fclean: clean
	@$(FTMAKE) fclean
	@$(DEL) $(NAME)
	@echo "$(APPDELETED)"

norm:
	@norminette $(SRC_DIR)
	@$(FTMAKE) norm

re: fclean all

.PHONY: all fclean clean re pre debug
